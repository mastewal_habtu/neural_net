from debug_utils import *
from net_utils import *


class Layer:
    # weight initialization distribution modes
    UNIFORM = 1
    NORMAL = 2

    # weight initialization variance guarding modes
    FAN_IN = 1
    FAN_OUT = 2
    FAN_BOTH = 3

    # activation functions mode
    SIGMOID = 1
    TANH = 2
    RELU = 3
    SOFTMAX = 4

    # input data normalization modes
    MIN_MAX = 1
    MIN_MAX_RANGE_EXTENDED = 2
    Z_SCORE = 3
    MAD_MEDIAN = 4
    DOUBLE_SIGMOID = 5
    TANH_HAMPEL = 6

    # loss function modes
    MEAN_SQUARE_ERROR = 1
    MEAN_SQUARE_LOG_ERROR = 2
    L2 = 3
    MEAN_ABSOLUTE_ERROR = 4
    L1 = 5
    # cross entropy per single class i.e especially in multi-class membership
    MULTI_CROSS_ENTROPY = 6
    # cross entropy per whole classes like binary interpretation of cross entropy
    #  i.e assumes output is 0 or 1
    MEAN_CROSS_ENTROPY = 7
    NEG_LOG_LIKELIHOOD = 8
    KL_DIVERGENCE = 9
    POISSON = 10
    HINGE = 11
    HINGE_SQUARE = 12
    COSINE_PROXIMITY = 13

    MOMENTUM = 1
    NESTOROV = 2

    def __init__(self, nodes, number=None, config=None):
        self.number = number
        self.nodes = nodes

        self.config = config

        self.weights = None
        self.biases = None
        self.activations = None
        self.delta = None

        self.prev = None
        self.next = None

        self.debug_weights = False

    def init_weights(self, distribution=NORMAL, variance_guard=FAN_BOTH, std=None, range=None, mean=0):
        """
        Initialize weights

        Args:

        """

        if self.weights is not None:
            print("Warning: Initializing weights which have values in layer {}".format(self.number))

        if distribution == Layer.NORMAL:
            if std is None:
                if variance_guard == Layer.FAN_BOTH:
                    # compromise fan_in and fan_out case using harmonic mean
                    good_variance = 2.0 / (self.prev.nodes + self.nodes)
                elif variance_guard == Layer.FAN_IN:
                    good_variance = 1.0 / self.prev.nodes
                elif variance_guard == Layer.FAN_OUT:
                    good_variance = 1.0 / self.nodes
                else:
                    raise ValueError("Unknown Variance Guard mode")

                if self.config.activation == Layer.RELU:
                    # actually, some very deep CNNs have difficulties to converge
                    # especially relus reason is expectation of square of x is not equal to variance of x
                    #                   unless x has zero mean
                    #  and makes sense since relus are zero for half of their inputs
                    good_variance = 2.0 * good_variance

                good_std = np.sqrt(good_variance)
            else:
                good_std = std

            self.weights = np.random.normal(mean, good_std, (self.prev.nodes, self.nodes))
            if self.debug_weights:
                print("good std in layer {}".format(self.number), good_std)
        elif distribution == Layer.UNIFORM:
            # based on variance satisfaction of normal distribution as above
            # based on Var(U(-a,a)) = a square / 3 for uniform distribution U(-a,a)
            if range is None:
                if variance_guard == Layer.FAN_BOTH:
                    good_range = 6.0 / (self.prev.nodes + self.nodes)
                elif variance_guard == Layer.FAN_IN:
                    good_range = 3.0 / self.prev.nodes
                elif variance_guard == Layer.FAN_OUT:
                    good_range = 3.0 / self.nodes
                else:
                    raise ValueError("Unknown Variance Guard mode")

                if self.config.activation == Layer.RELU:
                    # actually, some very deep CNNs have difficulties to converge
                    # especially relus reason is expectation of square of x is not equal to variance of x
                    #                   unless x has zero mean
                    #  and makes sense since relus are zero for half of their inputs
                    good_range = good_range * 2.0
                elif self.config.activation == Layer.SIGMOID:
                    # I have no clue why Xavier decided this one when sigmoid with uniform
                    # is the playing card
                    # Actually, my wish is multiplying the std by 4
                    # which means multiplying variance by 4 square
                    good_range = good_range * 16.0

                good_range = np.sqrt(good_range)
            else:
                good_range = range
            self.weights = np.random.uniform(-good_range, good_range, (self.prev.nodes, self.nodes))
            if self.debug_weights:
                debug_data('good range in layer {}'.format(self.number), good_range)
        else:
            raise ValueError("Unknown distribution specified in layer {}".format(self.number))

    def init_biases(self):
        if self.biases is not None:
            print("Warning: initializing already initialize biases in layer {}".format(self.number))

        self.biases = np.zeros((1, self.nodes))

    def init_velocity(self):
        if self.config.descent is not None:
            self.velocity = np.random.normal(0, 1, (self.prev.nodes, self.nodes))
        else:
            self.velocity = np.zeros((self.prev.nodes, self.nodes))

    def forward(self, X, k=0):
        """
        Forward input to next layer

        Args:
            X: input data
            k: maybe relu leaking constant

        Returns:
            output for next layer
        """

        if X.shape[1] != self.weights.shape[0]:
            debug_data('X',X)
            debug_data('weights', self.weights)
            raise ValueError("input shape does not conform with weight shape")

        transformed = X.dot(self.weights) + self.biases

        if self.config.activation == Layer.SIGMOID:
            self.activations = sigmoid(transformed)
        elif self.config.activation == Layer.TANH:
            self.activations = tanh(transformed)
        elif self.config.activation == Layer.RELU:
            self.activations = relu(transformed, self.config.k)
        elif self.config.activation == Layer.SOFTMAX:
            self.activations = softmax(transformed)
        else:
            raise ValueError("Unknown Activation Function to forward")

        return self.activations

    def deactivate(self, activation):
        """
        Derivate an activation function

        Args:
            activation: values used to derivate
            k: maybe leaky relu constant
        """

        if self.config.activation == Layer.SIGMOID:
            return sigmoid_derivate(activation)
        elif self.config.activation == Layer.TANH:
            return tanh_derivate(activation)
        elif self.config.activation == Layer.RELU:
            return relu_derivate(activation, self.config.k)
        elif self.config.activation == Layer.SOFTMAX:
            return softmax_derivate(activation)
        else:
            raise ValueError("Unknown mode of activation function to derivate")

    def compute_delta(self):
        """
        Delta of layer

        """

        self.delta = self.next.delta.dot(self.next.weights.T) * self.deactivate(self.activations)
        debug_data('layer {} weights'.format(self.number+1), self.next.weights)
        debug_data('layer {} activations'.format(self.number), self.activations)
        debug_data('layer {} deactivations'.format(self.number), self.deactivate(self.activations))
        debug_data('layer {} delta computed'.format(self.number), self.delta)
        return self.delta

    def update_weights(self, n):
        """
        update weights of current layer

        Args:
            n: number of data(rows) used to update weights
        """

        debug_data('layer {} weight diff'.format(self.number),
                   self.prev.activations.T.dot(self.delta) * self.config.learning_rate)
        if self.config.lamda > 0:
            if self.config.regularization == Layer.L2:
                self.weights -= (self.config.lamda * self.config.learning_rate / n) * self.weights
            elif self.config.regularization == Layer.L1:
                self.weights -= (self.config.lamda * self.config.learning_rate / n) * np.sign(self.weights)
            else:
                raise ValueError("Unknown regularization mode")


        if self.config.descent == Layer.MOMENTUM:
            self.velocity = self.config.friction * self.velocity - self.prev.activations.T.dot(self.delta) * self.config.learning_rate
        elif self.config.descent == Layer.NESTOROV:
            raise ValueError("Unsupported descenting i.e. Nestorov")
        self.weights += self.velocity

    def update_biases(self):
        debug_data('layer {} bias diff'.format(self.number),
                   np.sum(self.delta, axis=0, keepdims=True) * self.config.learning_rate)
        self.biases -= np.sum(self.delta, axis=0, keepdims=True) * self.config.learning_rate


class NetConfig:
    def __init__(self, **kwargs):
        self.layer_nodes = kwargs.get('layer_nodes')

        self.learning_rate = kwargs.get('learning_rate', 0.1)
        self.epoch = kwargs.get('epoch', 10)
        self.batch_size = kwargs.get('batch_size', None)

        self.should_normalize = kwargs.get('should_normalize', True)

        self.activation = kwargs.get('activation', Layer.SIGMOID)
        self.last_activation = kwargs.get('last_activation', self.activation)
        self.k = kwargs.get('k', 0)
        self.loss_function = kwargs.get('loss_function', Layer.MEAN_SQUARE_ERROR )

        self.use_biases = kwargs.get('use_biases', True)

        self.regularization = kwargs.get('regularization', Layer.L2)
        self.lamda = kwargs.get('lamda', 0)

        self.descent = kwargs.get('descent', Layer.MOMENTUM)
        self.friction = kwargs.get('friction', 0.8)


class NeuralNet:
    """
    Simple Neural Net with full whistle and bells
    """

    def __init__(self, config=NetConfig()):
        self.config = config
        self.empty_weights = True

        self.layers = []
        for nodes in self.config.layer_nodes:
            self.append(
                Layer(nodes=nodes, config=self.config)
            )

        self.layers[len(self.layers) - 1].config.activation = self.config.last_activation

    def append(self, layer):
        """
                Append new layer
                Args:
                    layer: new layer

                Returns:
                    nothing
                """

        layer.number = len(self.layers)

        layer.prev = self.layers[layer.number - 1] if layer.number != 0 else None
        if layer.prev is not None:
            layer.prev.next = layer

        layer.next = None

        self.layers.append(layer)

    def validate_X(self, X):
        """
        Validate input data

        Args:
            X: a two dimensional data with numbers

        Returns:
            A validated data that is cleaned if need to be

        Raises:
            ValueError: if data is invalid
        """

        if isinstance(X, np.ndarray):
            if len(X.shape) == 2:
                return X
            else:
                debug_data('X', X)
                raise ValueError("X is np.ndarray which is not two dimensional")
        elif isinstance(X, list):
            if len(X) != 0 and isinstance(X[0], list) and len(X[0]) != 0 and not isinstance(X[0][0], list):
                return np.array(X)
            else:
                debug_data('X', X)
                raise ValueError("X is a list which is not two dimensional")
        else:
            debug_data('X', X)
            raise ValueError("X is not a list or np.ndarray, can't work with that")

    def validate_y(self, y):
        """
        Validate output data

        Args:
            y: a one dimensional data with numbers

        Returns:
            A validated data that is cleaned if need to be

        Raises:
            ValueError: if data is invalid
        """

        if isinstance(y, np.ndarray):
            if len(y.shape) == 1:
                return y.reshape((y.shape[0], 1))
            elif len(y.shape) == 2 and y.shape[1] == 1:
                return y
            else:
                debug_data('y', y)
                raise ValueError("y is np.ndarray, which is not one dimensional")
        elif isinstance(y, list):
            if len(y) != 0 and not isinstance(y[0], list):
                return np.array(y).reshape(len(y), 1)
            else:
                debug_data('y', y)
                raise ValueError("y is list, which is not one dimensioanl")
        else:
            debug_data('y', y)
            raise ValueError("y is not list or np.ndarray, can't work with that")

    def normalize(self, X, mode=Layer.Z_SCORE, m=None, s1 = None, s2 = None):
        """
        Normalize input
        Args:
            X: input data
            mode: mode of normalization

        Returns:
            normalized data
        """

        if mode == Layer.Z_SCORE:
            mean = np.mean(X, axis=1)
            std = np.std(X, axis=1)
            col_X = (X.T - mean) / std
            return col_X.T
        elif mode == Layer.MIN_MAX:
            # in range (0,1) using min max normalization
            X_min = np.amin(X, axis=1)
            X_max = np.amax(X, axis=1)
            col_X = (X.T - X_min)/(X_max - X_min)
            return col_X.T
        elif mode == Layer.MIN_MAX_RANGE_EXTENDED:
            # in range (-1,1) using min max normalization
            X_min = np.amin(X, axis=1)
            X_max = np.amax(X, axis=1)
            col_X = (X.T - ((X_min + X_max)/2)) / ((X_max - X_min)/2)
            return col_X.T
        elif mode == Layer.MAD_MEDIAN:
            # robust in case of outliers since median is used
            X_median = np.median(X, axis=1)
            col_X_MAD = np.median(np.absolute(X.T - X_median), axis=0)
            col_X = (X.T - X_median)/ col_X_MAD
            return col_X.T
        elif mode == Layer.DOUBLE_SIGMOID:
            # double sigmoid function normalization, robust and efficient
            for i in range(len(X)):
                for j in range(len(X[i])):
                    if X[i,j] < m:
                        s = s1
                    else:
                        s = s2

                    X[i,j] = 1.0 / (1 + np.exp(-2 * ((X[i,j] - m) / s)))

            return X
        elif mode == Layer.TANH_HAMPEL:
            # tanh estimators using hampel estimators(robust and efficient)
            # (hampel is kind of filtering outliers)
            for i in range(len(X)):
                hampel_X = hampel(X[i])
                hampel_mean = np.mean(hampel_X)
                hampel_std = np.std(hampel_X)
                for j in range(len(X[i])):
                    X[i,j] = 0.5 * (np.tanh(0.01 * ((X[i,j] - hampel_mean)/hampel_std)) + 1)

            return X
        else:
            raise ValueError("Unknown normalization mode given")

    def init_weights(
            self, distribution=Layer.NORMAL, variance_guard=Layer.FAN_BOTH, std=None, range=None, mean=0):
        """
        Initialize weights of layers

        Returns:

        """

        for layer in self.layers[1:]:
            layer.init_weights(distribution,variance_guard,std,range,mean)

        self.empty_weights = False

    def init_biases(self):
        """
        Initialize biases with zeros

        """

        for layer in self.layers[1:]:
            layer.init_biases()

    def init_velocity(self):
        for layer in self.layers[1:]:
            layer.init_velocity()

    def loss(self, expected, output):
        if self.config.loss_function == Layer.MEAN_SQUARE_ERROR:
            return ((expected - output) ** 2).mean(axis=0)
        elif self.config.loss_function == Layer.MEAN_SQUARE_LOG_ERROR:
            # used when not want to penalize huge differences in the predicted and the actual values
            # when both predicted and true values are huge numbers
            return ((np.log(expected + 1) - np.log(output + 1)) ** 2).mean(axis=0)
        elif self.config.loss_function == Layer.L2:
            # same as mean square error without dividing the sum with the number of observations
            return ((expected - output) ** 2).sum(axis=0)
        elif self.config.loss_function == Layer.MEAN_ABSOLUTE_ERROR:
            # robust to outliers, difficult to compute gradient
            # mean square error appreciates big penalty for big error(difference)
            return (np.abs(expected - output)).mean(axis=0)
        elif self.config.loss_function == Layer.L1:
            # same as mean absolute error without dividing the sum with the number of observations
            return (np.abs(expected - output)).sum(axis=0)
        elif self.config.loss_function == Layer.MULTI_CROSS_ENTROPY:
            # multi class classification case
            # expected is one hot encoded vector
            num_examples = expected.shape[0]
            log_likelihood = -np.log(output[range(num_examples), expected])
            loss = np.sum(log_likelihood) / num_examples
            return loss
        elif self.config.loss_function == Layer.MEAN_CROSS_ENTROPY:
            # like binary interpretation of cross entropy i.e assumes output is 0 or 1
            # don't have problem as mean square error specieses with sigmoid kind of activation
            return -((expected * np.log(output) + (1 - expected) * np.log(1 - output)).mean(axis=0))
        elif self.config.loss_function == Layer.NEG_LOG_LIKELIHOOD:
            # a kind of most fitted with classifiers that output probability of each class
            # rather than most likely class
            return -((np.log(output)).mean(axis=0))
        elif self.config.loss_function == Layer.KL_DIVERGENCE:
            # also known as relative entropy, information divergence/gain
            # is entropy minus cross entropy(a kind of)
            return  (expected * np.log(expected)).mean(axis=0) - (expected * np.log(output)).mean(axis=0)
        elif self.config.loss_function == Layer.POISSON:
            return (output - (expected * np.log(output))).mean(axis=0)
        elif self.config.loss_function == Layer.HINGE:
            # used for “maximum-margin” classification, most notably for support vector machines (SVMs)
            return (np.maximum(0, (1 - expected * output))).mean(axis=0)
        elif self.config.loss_function == Layer.HINGE_SQUARE:
            # variant of hinge,
            # solves that the derivative of hinge loss has discontinuity at expected * output = 1
            return (np.maximum(0, (1 - expected * output)) ** 2).mean(axis=0)
        elif self.config.loss_function == Layer.COSINE_PROXIMITY:
            # is a measure of similarity between two non-zero vectors of an inner product space
            # that measures the cosine of the angle between them
            return -(
                (expected * output).sum(axis=0)
                /
                (np.sqrt((expected ** 2).sum(axis=0)) * np.sqrt((output ** 2).sum(axis=0)))
            )
        else:
            raise ValueError("Unknown mode of loss function")

    def deloss(self, expected, output):
        if self.config.loss_function == Layer.MEAN_SQUARE_ERROR:
            return -2 * (expected - output)
        elif self.config.loss_function == Layer.MULTI_CROSS_ENTROPY:
            raise ValueError(
                "No need to compute derivative of multivariate cross entropy loss function"
                " with respect to activated output"
            )
        elif self.config.loss_function == Layer.MEAN_CROSS_ENTROPY:
            raise ValueError(
                "No need to compute derivative of binaryvariate cross entropy loss function"
                " with respect to activated output"
            )
        else:
            raise ValueError(
                "Unsupported or unknown mode of loss function to derivate with respect to activated output"
            )

    def deactivated_deloss(self, expected, output):

        if self.config.loss_function == Layer.MEAN_SQUARE_ERROR:
            last_layer = self.layers[-1]
            return self.deloss(expected, output) * last_layer.deactivate(last_layer.activations)
        elif self.config.loss_function == Layer.MULTI_CROSS_ENTROPY:
            # expected is one hot encoded vector
            num_examples = expected.shape[0]
            output[range(num_examples), expected] -= 1
            grad = output / num_examples
            return grad
        elif self.config.loss_function == Layer.MEAN_CROSS_ENTROPY:
            # cross entropy per whole classes like binary interpretation of cross entropy
            #  i.e assumes output is 0 or 1
            return output - expected
        elif self.config.loss_function == Layer.NEG_LOG_LIKELIHOOD:
            return output - expected
        else:
            raise ValueError(
                "Unsupported or unknown mode of loss function to derivate with respect to non-activated output"
            )

    def feed_forward(self, X):

        self.layers[0].activations = X

        input = X
        output = None
        for layer in self.layers[1:]:
            output = layer.forward(input)
            input = output

        return output

    def back_propagate(self, expected):
        """
        Back propagation
        """

        last_layer = self.layers[len(self.layers) - 1]
        last_layer.delta = self.deactivated_deloss(expected, last_layer.activations)
        debug_data('last layer deactivated deloss', 'delta')
        debug_data('last layer activations', last_layer.activations)
        debug_data('last layer deactivations', last_layer.deactivate(last_layer.activations))
        debug_data('last layer delta', last_layer.delta)

        # exclude the last layer since it is already processed as above
        # exclude the first layer since it does not have any weight effectively
        for layer in self.layers[-2::-1]:
            # compute the layer delta if it is not input layer

            if layer.number != 0:
                layer.compute_delta()

            # update weights of the next layer since it is safe now
            # that means delta of prior layer is computed
            layer.next.update_weights( expected.shape[0])
            if self.config.use_biases:
                layer.next.update_biases()

    def train(self, X, y):
        """
        Train neural net
        """

        clean_X = self.validate_X(X)
        clean_y = self.validate_y(y)

        if self.config.should_normalize:
            clean_X = self.normalize(X)

        if self.empty_weights:
            self.init_weights()
            self.init_biases()

            self.init_velocity()

        if self.config.batch_size is None:
            self.config.batch_size = clean_X.shape[0]

        for iteration in range(self.config.epoch):
            for index in range(0, clean_X.shape[0], self.config.batch_size):
                self.feed_forward(clean_X[index:index + self.config.batch_size])
                self.back_propagate(clean_y[index:index + self.config.batch_size])

                # debug_data('activation output', self.layers[len(self.layers) - 1].activations)
                debug_data('deactivated deloss', self.deactivated_deloss(clean_y[index:index + self.config.batch_size], self.layers[len(self.layers) - 1].activations))
                debug_data('loss', self.loss(clean_y[index:index + self.config.batch_size], self.layers[len(self.layers) - 1].activations))
                # debug_data('index',index)
                # debug_data('batch size', self.config.batch_size)
                # debug_data('batch x', clean_X[index:self.config.batch_size])
                # debug_data('batch y', clean_y[index:self.config.batch_size])

    def predict(self, X, flatten=False):
        """
        Args:
            X: two dimensional input data
            flatten: make returned array one dimensional

        Returns:
            y: two dimensional output data with second dimension being one
        """

        clean_X = self.validate_X(X)

        if self.config.should_normalize:
            clean_X = self.normalize(X)

        prediction = self.feed_forward(clean_X)

        return prediction if not flatten else prediction.ravel()