import numpy as np
import sklearn
import sklearn.datasets
import sklearn.linear_model
import matplotlib.pyplot as plt

# Helper function to plot a decision boundary.
# If you don't fully understand this function don't worry, it just generates the contour plot below.
from debug_utils import debug_data
from neural_net import NeuralNet, NetConfig, Layer


def plot_decision_boundary(pred_func, X, y):
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)

def test_moon_data_with_two_classes(use_nn=True):
    np.random.seed(0)
    X, y = sklearn.datasets.make_moons(n_samples=200, noise=0.20)
    plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)

    if use_nn:
        clf = NeuralNet(
            NetConfig(
                layer_nodes=[2, 5, 1], should_normalize=False, loss_function=Layer.MEAN_SQUARE_ERROR,
                epoch=1000, learning_rate=0.1, activation=Layer.RELU, k=0.1
            )
        )
        clf.train(X, y)
    else:
        clf = sklearn.linear_model.LogisticRegressionCV()
        clf.fit(X, y)


    plot_decision_boundary(lambda x: clf.predict(x, flatten=True), X, y)
    plt.title("Logistic regression")

    plt.show(block=True)

def test_simple_data_set():
    # Test making predictions with the network
    dataset = np.array([[2.7810836, 2.550537003, 0],
               [1.465489372, 2.362125076, 0],
               [3.396561688, 4.400293529, 0],
               [1.38807019, 1.850220317, 0],
               [3.06407232, 3.005305973, 0],
               [7.627531214, 2.759262235, 1],
               [5.332441248, 2.088626775, 1],
               [6.922596716, 1.77106367, 1],
               [8.675418651, -0.242068655, 1],
               [7.673756466, 3.508563011, 1]])

    clf = NeuralNet(
        NetConfig(
            layer_nodes=[2,5,1],should_normalize=True, loss_function=Layer.MEAN_CROSS_ENTROPY,
            epoch=500, learning_rate=0.1, activation=Layer.SIGMOID, k=0.1)
    )
    clf.train(dataset[:,[0,1]], dataset[:,-1])

    for row in dataset:
        prediction = clf.predict(row[:-1].reshape((1,2)))
        print('Expected={}, Got={}'.format(row[-1], prediction))

def test_xor():
    # XOR example:

    training_sets = [
        [0, 0],
        [0, 1],
        [1, 0],
        [1, 1]
    ]

    answers = [
        0,
        1,
        1,
        0
    ]

    # champion parameters
    #

    np.random.seed(0)
    nn = NeuralNet(NetConfig(
        layer_nodes=[2, 2, 1], should_normalize=False, loss_function=Layer.MEAN_CROSS_ENTROPY,
        epoch=500, learning_rate=0.1, use_biases=True, activation=Layer.TANH, k=0.1,
        batch_size=None, last_activation=Layer.SIGMOID, lamda=0.1, descent=Layer.MOMENTUM, friction=0.7
    ))
    nn.train(training_sets, answers)

    nn.predict(training_sets)

if __name__ == '__main__':
    # test_moon_data_with_two_classes()

    # test_simple_data_set()

    test_xor()