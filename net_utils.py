import numpy as np


def hampel(x, k=3, nsigma=3):
    """
    adapted from hampel function in R package pracma
    x= 1-d numpy array of numbers to be filtered
    k= number of neighbours on either side; 3 is default
    nsigma= number of standard deviations to use; 3 is default
    """

    n = len(x)
    y = x  # y is the corrected series
    L = 1.4826
    for i in range((k + 1), (n - k)):
        if np.isnan(x[(i - k):(i + k + 1)]).all():
            continue
        x0 = np.nanmedian(x[(i - k):(i + k + 1)])
        S0 = L * np.nanmedian(np.abs(x[(i - k):(i + k + 1)] - x0))
        if np.abs(x[i] - x0) > nsigma * S0:
            y[i] = x0

    return y


def sigmoid(x):
    """
    Logistic sigmoid function

    """

    return 1 / (1 + np.exp(-x))


def sigmoid_derivate(x):
    """
    Logistic sigmoid function derivate

    """

    sgx = sigmoid(x)
    return sgx * (1 - sgx)


def tanh(x):
    # linearly related with sigmoid as 2 * sigmoid(2 * x) - 1
    return np.tanh(x)


def tanh_derivate(x):
    return 1 - np.tanh(x) ** 2


def relu(x, k=0):
    """
    Relu capable of being leaky
    Args:
        x: input
        k: leaking constant(best to be very small)

    """

    rx = x.copy()
    rx[rx < 0] = k * rx[rx < 0]

    return rx


def relu_derivate(x, k=0):
    """
        Relu derivate capable of being leaky
        Args:
            x: input
            k: leaking constant(best to be very small)

    """

    dx = x.copy()

    dx[dx > 0] = 1
    dx[dx < 0] = k
    dx[dx == 0] = 1

    return dx


def softmax(x):
    """
    Compute softmax in numerically stable way
    """

    shiftx = x - np.max(x)
    exps = np.exp(shiftx)

    return exps / np.sum(exps, axis=1, keepdims=True)


def softmax_derivate(x):
    """
    Only the diagonal part of the jacobian matrix of softmax
    i.e assuming i=j
    """

    return x * (1 - x)