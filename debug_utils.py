import numpy as np
import inspect

def debug_data(data_name='Data', data=None, show_type=True, trim=True, trim_size=10, tell_func=True):

    if tell_func:
        # inspect.currentframe() cpython implementation detail,
        # not guaranteed in all implementations of python
        current_frame = inspect.currentframe()
        caller_frame = inspect.getouterframes(current_frame, 2)
        print("-------------------------------------------------")
        print("In func: ", caller_frame[1][3])
        print("-------------------------------------------------")

    if show_type:
        print("{} type: ".format(data_name), type(data))

    tag = ''
    if isinstance(data, np.ndarray):
        print("{} shape: ".format(data_name), data.shape)
        if trim:
            data = data[:trim_size] if len(data.shape) == 1 else data[:trim_size,:trim_size]
            tag = '(trimmed)'

    print("{}{}: ".format(data_name, tag), data)
